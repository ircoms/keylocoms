------------------------------------------------------------------------------------------
This README explains the aim of the project with a description, the authors and license,
the requirements, how to use it and some additionnal informations.
If you have any question, please refer to additional information to send us an email
------------------------------------------------------------------------------------------

--------------------------
1. Project Description
2. Requirements
3. Utilization
4. Additional information
--------------------------

----------------------
1. Project Description
----------------------
This project is a keylogger developped with Powershell v2.0 under License GPLv3 [please read License.txt]
by 3 students from University of Valenciennes in France [pleaser read contributors.txt].
The project named Keylocoms display the key pressed by a computer in streaming by a client/server
TCP connection to an another computer in the same network.

----------------
2. Requirements
----------------
The use this project to need Powershell v2.0 and the .Net 3.5 Framework with 2 computers with Windows.
This project has been test with 2 windows 7 (premium edition) with default powershell and .NET installation

---------------
3. Utilization
---------------
a.  To start the script to capture the key pressed on a computer do as follow:
    - Start a Windows command prompt
    - Execute: powershell.exe keylocoms_server.ps1
    This will execute the script in the powershell environnement and waiting for client to connect

b.  To start the script to retrieve the key pressed on an another computer do as follow:
    - Start a Windows command prompt
    - Execute: powershell.exe keylocoms_client.ps1